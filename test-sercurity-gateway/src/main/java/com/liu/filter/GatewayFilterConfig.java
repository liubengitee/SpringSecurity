package com.liu.filter;

import cn.hutool.core.map.MapUtil;
import cn.hutool.http.HttpStatus;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

/**
 * 自定义路由过滤器
 *路由过滤器可用于修改进入的http请求和返回的http相应，路由过滤器只能指定路由进行使用
 */
@Component
@Slf4j
public class GatewayFilterConfig implements GlobalFilter, Ordered {
    @Autowired
    private TokenStore tokenStore;
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        String requestUrl = exchange.getRequest().getPath().value();
        AntPathMatcher pathMatcher = new AntPathMatcher();
        // 1.uaa的所有服务进行放行  pathMatcher.match("/uaa/**",requestUrl)路径是否匹配
        if(pathMatcher.match("/uaa/**",requestUrl)){
          return chain.filter(exchange);
        }
        //2.检查token是否存在
        //第一次进来的时候没有token，第二次访问的时候从uaa微服务中获取到了token
        String token = getToken(exchange);
        //System.out.println("token:"+token);
        if(token == null){
           return noTokenMono(exchange);
        }
        //3.判断是不是有效的token
        OAuth2AccessToken oAuth2AccessToken;
        try {
            oAuth2AccessToken = tokenStore.readAccessToken(token);
            Map<String, Object> additionalInformation = oAuth2AccessToken.getAdditionalInformation();
            //取出用户身份信息
            String principal  = MapUtil.getStr(additionalInformation, "user_name");
            //获取用户的权限
            Object authorities = additionalInformation.get("authorities");

            JSONObject jsonObject=new JSONObject();
            jsonObject.put("principal",principal);
            jsonObject.put("authorities",authorities);
            //给header里面添加值
            //String encode = new BCryptPasswordEncoder().encode(jsonObject.toJSONString());  //加密
            ServerHttpRequest tokenRequest = exchange.getRequest().mutate().header("json-token", jsonObject.toJSONString()).build();
            ServerWebExchange build = exchange.mutate().request(tokenRequest).build();
            return chain.filter(build);
        }catch (InvalidTokenException e){
            log.info("无效的token: {}", token);
            return invalidTokenMono(exchange);
        }

    }

    @Override
    public int getOrder() {
        return 0;
    }

    /**
     * 获取token
     * @param exchange
     * @return
     */
    private String getToken(ServerWebExchange exchange){
        List<String> list = exchange.getRequest().getHeaders().get("Authorization");
        if(list==null){
            return null;
        }
        String tokenStr = list.get(0);
        if (StringUtils.isEmpty(tokenStr)){
            return null;
        }
        String token = tokenStr.split(" ")[1];
        return token;
    }

    /**
     * 没有token
     * @param exchange
     * @return
     */
    private Mono<Void> noTokenMono(ServerWebExchange exchange){
        JSONObject json = new JSONObject();
        json.put("status", HttpStatus.HTTP_UNAUTHORIZED);
        json.put("data","没有token");
        return jsonToMono(json,exchange);
    }

    /**
     * 将json字符串转换为Mono的格式,并添加到相应中去
     * @param json
     * @param exchange
     * @return
     */
    private Mono<Void> jsonToMono(JSONObject json, ServerWebExchange exchange){
        ServerHttpResponse response = exchange.getResponse();
        byte[] bytes = json.toJSONString().getBytes(StandardCharsets.UTF_8);
        DataBuffer dataBuffer = response.bufferFactory().wrap(bytes);
        response.setStatusCode(org.springframework.http.HttpStatus.valueOf(HttpStatus.HTTP_UNAUTHORIZED));
        //指定编码，否则在浏览器中会中文乱码
        response.getHeaders().add("Content-Type", "text/plain;charset=UTF-8");
        return response.writeWith(Mono.just(dataBuffer));
    }

    /**
     * 无效的token
     * @param exchange
     * @return
     */
    private Mono<Void> invalidTokenMono(ServerWebExchange exchange){
        JSONObject json = new JSONObject();
        json.put("status",HttpStatus.HTTP_UNAUTHORIZED);
        json.put("data", "无效的token");
        return jsonToMono(json, exchange);
    }

}
