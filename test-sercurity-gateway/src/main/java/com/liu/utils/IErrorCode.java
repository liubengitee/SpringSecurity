package com.liu.utils;

public interface IErrorCode {
    long getCode();
    String getMessage();
}
