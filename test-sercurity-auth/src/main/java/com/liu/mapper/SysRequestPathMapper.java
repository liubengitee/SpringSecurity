package com.liu.mapper;

import com.liu.pojo.SysRequestPath;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
* @Entity com.liu.pojo.SysRequestPath
*/
@Mapper
public interface SysRequestPathMapper extends BaseMapper<SysRequestPath> {


}
