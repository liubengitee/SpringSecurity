package com.liu.mapper;

import com.liu.pojo.SysUser;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
* @Entity com.liu.pojo.SysUser
*/
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {
    List<SysUser> selectByUserNameSysUsers(String userName);
}
