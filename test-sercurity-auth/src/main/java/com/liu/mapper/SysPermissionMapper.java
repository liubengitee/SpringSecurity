package com.liu.mapper;

import com.liu.pojo.SysPermission;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
* @Entity com.liu.pojo.SysPermission
*/
@Mapper
public interface SysPermissionMapper extends BaseMapper<SysPermission> {


}
