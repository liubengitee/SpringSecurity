import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


public class BCryptTest {
    public static void main(String[] args) {
        BCryptPasswordEncoder bcp = new BCryptPasswordEncoder();
        // 原始密码（明文密码）
        String rawPassword = "123456";
        // 使用 BCrypt 对明文加密
        String encodedPassword = bcp.encode(rawPassword);
        System.out.println(encodedPassword);
        // 校验密码
        if (bcp.matches(rawPassword, encodedPassword)) {
            System.out.println("密码校验成功");
        } else {
            System.out.println("密码错误");
        }
    }
}
